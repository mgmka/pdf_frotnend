
module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'front',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: 'pdf frontend'}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'}
    ]
  },
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth'
  ],

  axios: {
    proxy: true
    // baseURL: 'http://localhost:8000'
  },
  proxy: {
    '/api': 'https://bestnail.design'
  },

  auth: {
    redirect: {
      callback: '/callback',
      user: '/panel',
      logout: '/panel',
      home: '/panel'
    },
    strategies: {
      local: {
        endpoints: {
          login: { url: '/api-rest-auth/login/', method: 'post', propertyName: 'key' },
          user: { url: '/api/users/profile/', method: 'get', propertyName: 'username' },
          logout: {url: '/api-rest-auth/logout/', method: 'post'}
        },
        tokenType: 'Token'
      }
    }
  },
  plugins: ['~/plugins/vuetify.js'],
  css: [
    '~/assets/style/app.styl'
  ],
  /*
    ** Customize the progress bar color
    */
  loading: {color: '#fefaff'},
  /*
    ** Build configuration
    */
  build: {
    vendor: [
      '~/plugins/vuetify.js'
    ],
    extractCSS: true,
    /*
      ** Run ESLint on save
      */
    extend (config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        })
      }
    }
  }
}